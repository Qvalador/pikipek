# -*- coding: utf-8 -*-

import discord
import discord.utils
from discord.ext import commands
import asyncio
import random
import os
import time
from plugins.util import permissions
from conf import *

description = '''a general purpose bot.  how did you find this description, anyway?'''
bot = commands.Bot(command_prefix='?', description=description, pm_help=True)

plugins = [
    'plugins.rng',
    'plugins.tgrp',
    'plugins.admin',
    'plugins.market',
    'plugins.music',
    'plugins.fort',
    'plugins.starfall',
]

@bot.event
async def on_ready():
    print('connected!')
    print('username: ' + bot.user.name)
    print('id: ' + bot.user.id)
    # update avatar
    picture = "avatar.jpg"
    if os.path.isfile(picture):
        with open(picture, "rb") as avatar:
            await bot.edit_profile(avatar=avatar.read())
            print("bot avatar set.")
    # load admins
    permissions.load_admins()
    # load plugins        
    for plugin in plugins:
        try:
            bot.load_extension(plugin)
            print("loaded {0}.".format(plugin[8:]))
        except Exception as e:
            print('failed to load plugin {}\n{}: {}'.format(plugin, type(e).__name__, e))
            
@bot.command()
@permissions.is_owner()
async def reload(cogname):
    cog = "plugins." + cogname
    try:
        bot.unload_extension(cog)
        bot.load_extension(cog)
        print('reloaded {}'.format(cogname))
        await bot.say('reloaded {}.'.format(cogname))
    except Exception as e:
        print('failed to reload plugin {}\n{}: {}'.format(cogname, type(e).__name__, e))
    
@bot.event
async def on_message_edit(old, new):
    await bot.process_commands(new)
    
bot.run(token)
