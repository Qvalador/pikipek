# -*- coding: utf-8 -*-

import discord
import re
import sys

from io import StringIO
from plugins.util import permissions
from discord.ext import commands
from conf import *


class Admin():
    """Utilities usable only by admin users."""
    def __init__(self, bot):
        self.bot = bot
        self.admins_dir = admins_dir
        self.admins = []
        self.admins = open(self.admins_dir).readlines()
        for i in range(len(self.admins)):
            self.admins[i] = self.admins[i].rstrip('\n')
        
    @commands.group(pass_context=True)
    async def admin(self, ctx):
        """completes a variety of administrative tasks."""
        if ctx.invoked_subcommand is None:
            await self.bot.say('incorrect random subcommand passed.')
            
    @commands.command()
    @permissions.is_admin()
    async def ping(self):
        """responds with 'pong!'"""
        await self.bot.say('pong!')

    @commands.command()
    @permissions.is_admin()
    async def say(self, destination: discord.Channel, *args):
        msg = " ".join(args)
        await self.bot.send_message(destination, msg)
        
    @commands.command()
    @permissions.is_admin()
    async def tell(self, user: discord.Member, *args):
        msg = " ".join(args)[:-1]
        await self.bot.send_message(user, msg) 
        
    @admin.command()
    @permissions.is_owner()
    async def add(self, raw_mentions):
        """adds the given user to the list of admins."""
        usr = re.sub("[^0-9]", "", raw_mentions)
        if usr not in self.admins:
            self.admins.append(usr)
            with open(self.admins_dir, "a") as f:
                f.writelines('\n' + usr)
                print('{0} has been added as an admin.'.format(usr))
            await self.bot.say('{0} has been added as an admin.'.format(raw_mentions))
        elif usr in self.admins:
            await self.bot.say('{0} is already an admin.'.format(raw_mentions))
           
    @commands.group()
    async def cool(self, raw_mentions):
        """says if a user is cool."""
        if re.sub("[^0-9]", "", raw_mentions) in permissions.admins:
            await self.bot.say('yes, {0} is cool'.format(raw_mentions))
        else:
            await self.bot.say('no, {0} is not cool'.format(raw_mentions))

    @cool.command(name='pikipek')
    async def _bot():
        """is the bot cool?"""
        await bot.say("yes, i'm the bomb.")
        
    @commands.command()
    async def joined(self, member : discord.Member):
        """says when a member joined."""
        await self.bot.say('{0.name} joined on {0.joined_at}'.format(member).lower())
        

    @commands.command(name="exec", hidden=True, pass_context=True, enabled=True)
    @permissions.is_owner()
    async def execute(self, ctx, *, code: str):
        """If you thought eval was dangerous, wait'll you see exec!"""
        code = code.strip("```").lstrip("python")
        result = None
        env = {}
        env.update(locals())
        stdout = sys.stdout
        redirect = sys.stdout = StringIO()

        try:
            exec(code, globals(), env)
        except Exception as err:
            await self.bot.say(python.format(type(err).__name__ + ": " + str(error)))
        finally:
            sys.stdout = stdout

        await self.bot.say("```\n{}\n```".format(redirect.getvalue()))

def setup(bot):
    bot.add_cog(Admin(bot))
