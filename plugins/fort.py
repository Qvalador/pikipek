# -*- coding: utf-8 -*-

from discord.ext import commands
from .util import permissions
from .fortstuff import auxiliary as aux
import os
import discord
import pickle
import re

class Fort:
    """let's play a game."""
    
    def __init__(self, bot):
        self.bot = bot
        self.games = {}
        self.server = self.bot.get_server('277920477190815744')
        for filename in os.listdir("/home/qvalador/Code/pikipek/plugins/fortstuff/games/"):
            gamechanname = filename[:-5]
            gamechan = self.server.get_channel(gamechanname)
            self.games[gamechanname] = pickle.load( open("/home/qvalador/Code/pikipek/plugins/fortstuff/games/{}".format(filename), "rb") )
            print("a fort game at {} has been loaded.".format(filename))
        
    def get_id(self, _id):
        return re.sub("[^0-9]", "", _id)  # get the raw user id by stripping everything that isn't a number.
        
    def get_name(self, usr: discord.User):
        if usr.nick:
            return usr.nick
        else:
            return usr.name
       
    @commands.command()       
    async def test(self, chan: discord.Channel):
        chanid = self.get_id(chan.id)
        await self.bot.say(self.games[chanid][0].players)
        
    @commands.group(pass_context=True)
    async def make(self, ctx):
        """makes various fort things."""
        if ctx.invoked_subcommand is None:
            await self.bot.say('incorrect random subcommand passed.')
        
    @make.command(pass_context=True)
    async def game(self, ctx, chan: discord.Channel):
        """starts a game in the provided channel."""
        chan_id = self.get_id(chan.id)
        usr_id = self.get_id(ctx.message.author.id)
        if chan_id not in self.games:
            self.games[chan_id] = [aux.Game(), chan] #add the game to the dictionary by channel id, then register the game and channel objects
            self.games[chan_id][0].channels.append(chan) #add the channel to the game object
            self.games[chan_id][0].players[usr_id] = aux.Player(usr_id) #create a player instance for this game and put it in the game object
            self.games[chan_id][0].players[usr_id].name = self.get_name(ctx.message.author)
            await self.bot.say("a game has been created in {}!".format(chan))
        else:
            await self.bot.say("there is already a game going in {}.".format(chan))
        
    @make.command(pass_context=True)
    async def team(self, ctx, chan: discord.Channel, *args):
        """creates a team of name 'teamname' and opens it for joining."""
        teamname = " ".join(args) # turn *args into an actual string
        chan_id = self.get_id(chan.id)
        usr = self.get_id(ctx.message.author.id)
        if self.games and chan_id in self.games: # don't make a team for a non-game
            teamrole = await self.bot.create_role(self.server, name=teamname)
            await self.bot.add_roles(ctx.message.author, teamrole)
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            allowed_perms = discord.PermissionOverwrite(read_messages=True)
            role_location = self.server.roles.index(teamrole)
            everyone = discord.ChannelPermissions(target=self.server.default_role, overwrite=everyone_perms)
            allowed = discord.ChannelPermissions(target=self.server.roles[role_location], overwrite=allowed_perms)
            gamechan = await self.bot.create_channel(self.server, teamname.lower().replace(" ", "_"), everyone, allowed)
            #self.games[chan_id][0].teams = {teamname: [
            await self.bot.say("you have founded and joined team {}.".format(teamname))
            #everyone = discord.PermissionOverwrite(read_messages=False)
            #mine = discord.PermissionOverwrite(read_messages=True)
            #await self.bot.create_channel(server, 'secret', (self.server.default_role, everyone), (self.server.me, mine))
        else:
            await self.bot.say("make a game in that channel first.")
            
    @commands.group(pass_context=True)
    async def join(self, ctx):
        """joins various fort things."""
        if ctx.invoked_subcommand is None:
            await self.bot.say('incorrect random subcommand passed.')
            
    @join.command(pass_context=True)
    async def game(self, ctx, chan: discord.Channel):
        """joins the indicated game.  kind of burdensome but how else am i going to make a player object."""
        chan_id = self.get_id(chan.id)
        usr_id = self.get_id(ctx.message.author.id)
        self.games[chan_id][0].players[usr_id] = aux.Player(usr_id) #create a player instance for this game and put it in the game object
        self.games[chan_id][0].players[usr_id].name = self.get_name(ctx.message.author)
        
            
    @join.command(pass_context=True)
    async def team(self, ctx, *args):
        """joins the selected team.  currently it must match exactly, but isn't case sensitive."""
        teamname = " ".join(args) # turn *args into an actual string
        

    @commands.command()                                                                            
    async def savegame(self, gamechan: discord.Channel):                                           
        """saves the game based in the current channel to a pickle file for later use."""          
        chan_id = self.get_id(gamechan.id)                                                         
        filename = "/home/qvalador/Code/pikipek/plugins/fortstuff/games/{}.game".format(chan_id)
        if chan_id in self.games:
            pickle.dump( (self.games[chan_id]), open(filename, "w+b") )
            await self.bot.say("saved game as `{}.game`.".format(chan_id))

#######################################################################################################
#######################################################################################################            
##              don't use this unless you have to.  he loads all games on startup.                   ##            
##    @commands.command()                                                                            ##
##    async def loadgame(self, gamechan: discord.Channel):                                           ##
##        chan_id = self.get_id(gamechan.id)                                                         ##
##        gamedir = "/home/qvalador/Code/pikipek/plugins/fortstuff/games/{}.game".format(chan_id)    ##
##        if os.path.isfile(gamedir):                                                                ##
##            self.games[chan_id] = pickle.load( open(gamedir, "rb") )                               ##
##            await self.bot.say("the game at {} has been loaded.".chan)                             ##
##        else:                                                                                      ##
##            await self.bot.say("there's no game saved there.")                                     ##
#######################################################################################################
#######################################################################################################        
            
    @commands.command()
    @permissions.is_owner()
    async def clear(self):
        """deletes all game-related objects."""
        channel_queue = []
        role_queue = []
        number_of_things = 0
        for chan in self.server.channels:
            if chan.name not in ["development", "general", "game"]:
                channel_queue.append(chan)
                number_of_things += 1
        for role in self.server.roles:
            if role.name not in ["VIB (Very Important Bird)", "Developers", "@everyone"]:
                role_queue.append(role)
                number_of_things += 1
        for thing in role_queue:
#            await self.bot.say("{} {}".format(thing, thing.id))
            await self.bot.delete_role(self.server, thing)
        for stuff in channel_queue:
            await self.bot.delete_channel(stuff)
        await self.bot.say("cleared {} objects successfully.".format(number_of_things))
           
def setup(bot):
    bot.add_cog(Fort(bot))  
