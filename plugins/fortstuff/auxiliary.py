class Team:
    """the class for team objects.  it tracks things like the relevant role and channel, as well as game-related attributes."""
    
    def __init__(self, name, channel, role, game):
        self.name = ""
        self.hp = 50
        self.type = ""
        self.players = {}
        self.actions = "something"
        self.channel = channel
        self.role = role
        self.game = game
        
    def __str__(self):
        return "Team {0}: [{1}HP, Type{2}, Players:{3}".format(self.name, self.hp, self.type, self.players)
        
class Game:
    """the class for game objects.  contains information like the teams, turn, relevant channels, and players."""
    
    def __init__(self):
        self.teams = {}
        self.turn = 0
        self.channels = []
        self.players = {}
        
    def __str__(self):
        return ( "Game @ {0}: [Teams:{1}, Players:{2}]".format(self.channels, self.teams, self.players) )
        
class Player:
    """the class for player objects.  tracks basic stuff like the discord user's id, name, and nick, in addition to their game and team."""
    
    def __init__(self,  usrid):
        self.id = usrid
        self.name = ""
        self.team = None
