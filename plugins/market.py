# -*- coding: utf-8 -*-
# TO DO: add rep

from discord.ext import commands
from .util import permissions
import discord
import re
import csv
from conf import *

class Market:
    """does some random annoying shit."""
    
    def __init__(self, bot):
        self.bot = bot
        self.dir = plugin_data_dir
        with open(self.dir + "debt.csv") as f:
            reader = csv.reader(f)
            self.debt = {line[0]: [float(line[1]), line[2], int(line[3]), line[4]] for line in reader}
        self.okay_servers = {"117494259011026947": self.bot.get_channel("198895987710885889"), "188585792350846976": self.bot.get_channel("269145633506459658")}
        with open(self.dir + "ranks.csv") as c:
            reader = csv.reader(c)
            self.ranks = {int(line[0]): line[1] for line in reader}
        with open(self.dir + "trigger_words.csv") as v:
            reader = csv.reader(v)
            self.trigger_words = {line[0]: int(line[1]) for line in reader}
        self.inflation = (250000 / sum(i[0] for i in list(self.debt.values())))
        self.prestige_emblem = u'\U00002B50'
            
    def save_debt_data(self):
        """saves the debt dict to the csv indicated in __init__."""
        _list = [[key, self.debt[key][0], self.debt[key][1], self.debt[key][2], self.debt[key][3]]  for key in self.debt]
        with open(self.dir + "debt.csv", "w") as f:
            writer = csv.writer(f)
            writer.writerows(_list)

    def check_if_okay(self, message):
        """checks if the message has the requirements needed to run market functions."""
        if message.server:
            if message.server.id in self.okay_servers:
                if message.author.id != "269130181564694539":
                    return True
        else:
            pass
        
        
    def get_name(self, member: discord.Member):
        """gets the usable name from a user."""
        if member.nick:
            return member.nick
        elif not member.nick:
            return member.name
        
    def money_format(self, n):
        """formats the number into money."""
        num = float(n)
        return '{0:.2f}'.format(num)

    @commands.command()
    async def thing(self):
        await self.bot.say(self.trigger_words)
    
    @commands.group(invoke_without_command=True, pass_context=True)
    async def balance(self, ctx, member: discord.Member=None):
        """various balance-related tasks.  if a subcommand is not passed, it returns the user's balance."""
        if member:
            usr = re.sub("[^0-9]", "", member.id)  # get the raw user id by stripping everything that isn't a number.
            try:
                if self.debt[usr][0] >= 0:
                    sign = "$"
                elif self.debt[usr][0] < 0:
                    sign = "-$"
                await self.bot.say("{0}'s balance is {2}{1}.".format(self.get_name(member),self.money_format(abs(self.debt[usr][0])),sign))
            except KeyError:
                await self.bot.say("that user doesn't have an account.  try saying more stuff and come back later.")
        else:
            usr = re.sub("[^0-9]", "", ctx.message.author.id)  # get the raw user id by stripping everything that isn't a number.   
            try:     
                if self.debt[usr][0] >= 0:
                    sign = "$"
                elif self.debt[usr][0] < 0:
                    sign = "-$"
                await self.bot.say("{0}'s balance is {2}{1}.".format(self.get_name(ctx.message.author),self.money_format(abs(self.debt[usr][0])),sign))
            except KeyError:
                await self.bot.say("that user doesn't have an account.  try saying more stuff and come back later.")        
            
    @balance.command()
    async def stats(self):
        """returns some useful information about the state of the bank."""
        await self.bot.say("The current inflation rate is {0:.3f}.  The current wealthiest member is {1}.  The current poorest member is {2}.".format(self.inflation, self.debt[(max(self.debt, key=lambda i: self.debt[i][0]))][3], self.debt[(min(self.debt, key=lambda i: self.debt[i][0]))][3]))
        
    @commands.command()
    async def bank(self):
        """spits out the entire debt dict, for troubleshooting purposes."""
        await self.bot.say(self.debt)
        
    @balance.command()
    @permissions.is_owner()
    async def bankrupt(self, member: discord.Member):
        """sets a user's balance to 0.  must be owner."""
        usr = re.sub("[^0-9]", "", member.id)  # get the raw user id by stripping everything that isn't a number.
        self.debt[usr][0] = 0
        self.debt[usr][1] = "0: Scrub"
        self.save_debt_data()
        await self.bot.say("{0}'s balance has been reset.  be responsible this time!".format(self.get_name(member)))
        
    @balance.command()
    @permissions.is_owner()
    async def charge(self, member: discord.Member, amount):
        """takes money away.  egad!"""
        usr = re.sub("[^0-9]", "", member.id)  # get the raw user id by stripping everything that isn't a number.
        if usr in self.debt:
            self.debt[usr][0] -= float(amount)
            self.save_debt_data()
        elif usr not in self.debt:
            self.debt[usr] = [float(amount) * -1,"0: Scrub",0,self.get_name(member)]
            self.save_debt_data()
        await self.bot.say("${0} has been removed from {1}'s account.  don't be such a shitter!".format(self.money_format(amount), self.get_name(member)))
        
    @balance.command()
    @permissions.is_owner()
    async def award(self, member: discord.Member, amount):
        """gives money.  naisu!"""
        usr = re.sub("[^0-9]", "", member.id)  # get the raw user id by stripping everything that isn't a number.
        if usr in self.debt:
            self.debt[usr][0] += float(amount)
            self.save_debt_data()
        elif usr not in self.debt:
            self.debt[usr] = [float(amount),"0: Scrub",0,self.get_name(member)]
            self.save_debt_data()
        await self.bot.say("${0} has been added to {1}'s account.  good work!".format(self.money_format(amount), self.get_name(member)))
        
    @balance.command(pass_context=True)
    async def give(self, ctx, member: discord.Member, amount):
        """transfers a specified amount of money from one account to another."""
        giver = re.sub("[^0-9]", "", ctx.message.author.id)  # get the raw user id by stripping everything that isn't a number.
        receiver = re.sub("[^0-9]", "", member.id)  # get the raw user id by stripping everything that isn't a number.
        if float(amount) > 0 and self.debt[giver][0] > 0:
            if giver and receiver in self.debt:
                if float(amount) <= self.debt[giver][0]:
                    self.debt[giver][0] -= float(amount)
                    self.debt[receiver][0] += float(amount)
                    self.save_debt_data()
                    await self.bot.say("${0} has been transferred from {1} to {2}'s account.  how nice!".format(float(amount), self.get_name(ctx.message.author), self.get_name(member)))
                elif float(amount) > self.debt[giver][0]:
                    await self.bot.say("you may not give away money you don't have.")
            elif giver not in self.debt or receiver not in self.debt:
                await self.bot.say("one of those users does not have an account.  try saying more stuff and come back later.")
        elif float(amount) <= 0 or self.debt[giver] <= 0:
            await self.bot.say("the amount given must be greater than zero, and the giver may not be in debt.")
            
    @balance.command(pass_context=True) # it's balance.command, not balance.commands.   STOP MAKING THAT MISTAKE YOU FOOL
    async def prestige(self, ctx):
        """allows the user to "prestige," resetting their balance but awarding an indicatory star."""
        usr = re.sub("[^0-9]", "", ctx.message.author.id)  # get the raw user id by stripping everything that isn't a number.
        if self.debt[usr][1] == "8: The Tokyo Ghoul":
            await self.bot.say("are you sure you'd like to prestige?  remember, this will reset your balance, and it might not be easy to get back.  respond with yes or no.")
            msg = await self.bot.wait_for_message(author=ctx.message.author)
            if "yes" in msg.content.lower():
                self.debt[usr] = [0, "0: Scrub", self.debt[usr][2] + 1]
                self.save_debt_data()
                await self.bot.say("congratulations, you've been increased to prestige rank {}.".format(self.debt[usr][2]))
            else:
                await self.bot.say("okay.  let me know if you change your mind.")
        else:
            await self.bot.say("you aren't of the requisite level.  come back later.")
            
    @commands.group(invoke_witout_command=True, pass_context=True)
    async def level(self, ctx, member: discord.Member=None):
        """allows a user to level up.  if no subcommand is passed, it returns the provided user's current level."""
        if member:
            usr = re.sub("[^0-9]", "", member.id)  # get the raw user id by stripping everything that isn't a number.
            try:
                await self.bot.say("{0} has the rank {2}**{1}**.".format(self.get_name(member), self.debt[usr][1], (self.prestige_emblem * self.debt[usr][2])))
            except KeyError:
                await self.bot.say("that user doesn't have an account.  try saying more stuff and come back later.")
        else:
            usr = re.sub("[^0-9]", "", ctx.message.author.id)  # get the raw user id by stripping everything that isn't a number.
            try:
                await self.bot.say("{0} has the rank {2}**{1}**.".format(self.get_name(ctx.message.author), self.debt[usr][1], (self.prestige_emblem * self.debt[usr][2])))
            except KeyError:
                await self.bot.say("that user doesn't have an account.  try saying more stuff and come back later.")            
        
    @commands.command(pass_context=True)
    async def levelup(self, ctx):
        """levels the user up.  yay!"""
        try:
            usr = re.sub("[^0-9]", "", ctx.message.author.id)  # get the raw user id by stripping everything that isn't a number.
            num = self.debt[usr][0]
            if num >= 0:
                next_level = max(filter(lambda x: num >= x,self.ranks)) #http://stackoverflow.com/a/25813098
            elif num < 0:
                next_level = min(filter(lambda x: num < x,self.ranks))
            if self.debt[usr][1] == self.ranks[next_level]:
                await self.bot.say("you cannot afford a promotion at this time.  come back again later!")
                return
            else:
                await self.bot.say("would you like to increase to rank **{0}**? (respond with yes or no)".format(self.ranks[next_level]))
                msg = await self.bot.wait_for_message(author=ctx.message.author)
                if "yes" in msg.content.lower():
                    self.debt[usr][1] = self.ranks[next_level]
                    self.save_debt_data()
                    await self.bot.say("you have been advanced to {1}**{0}**.".format(self.debt[usr][1], (self.prestige_emblem * self.debt[usr][2])))
                else:
                    await self.bot.say("okay.  let me know if you change your mind.")
        except KeyError:
            await self.bot.say("that user doesn't have an account.  try saying more stuff and come back later.")
        
    async def on_message(self, message):
        """hoo boy."""
        content = message.content.lower()
        author = message.author
        usr = re.sub("[^0-9]", "", author.id)  # get the raw user id by stripping everything that isn't a number.
        ##### Furry Repellant ######
        if self.check_if_okay(message):
            dest = self.okay_servers[message.server.id]     
            for word in self.trigger_words:
                if word in content:
                        if usr not in self.debt:
                            self.debt[usr] = [(self.trigger_words[word] * self.inflation), "0: Scrub", 0, self.get_name(author)]
                            self.save_debt_data()                            
                        elif usr in self.debt:
                            self.debt[usr][0] += (self.trigger_words[word] * self.inflation)
                            self.save_debt_data()
        elif not self.check_if_okay(message):
            pass
                
def setup(bot):
    bot.add_cog(Market(bot))                
