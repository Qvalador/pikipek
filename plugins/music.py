# -*- coding: utf-8 -*-

from discord.ext import commands
import discord

class Music:
    """plays music into voice channels."""
    
    def __init__(self, bot):
        self.bot = bot
        self.voice = None
        #discord.opus.load_opus('/usr/share/doc/libopus0')
        
    @commands.group(pass_context=True)
    async def music(self, ctx):
        """does some music magic."""
        if ctx.invoked_subcommand is None:
            await self.bot.say('incorrect music subcommand passed.')
            
    @music.command(pass_context=True)
    async def join(self, ctx):
        vchan = ctx.message.author.voice_channel
        self.voice = await self.bot.join_voice_channel(vchan)
        
    @music.command()
    async def leave(self):
        await self.voice.disconnect()
        
    @music.command()
    async def play(self):
        if self.voice.is_connected():
            player = self.voice.create_ffmpeg_player('/home/qvalador/Music/02 Dark Necessities.m4a')
            player.start()
            
def setup(bot):
    bot.add_cog(Music(bot))