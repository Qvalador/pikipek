# -*- coding: utf-8 -*-
#Starfall v1.1
import asyncio
import csv

from discord.ext import commands
from discord import Channel
from conf import *

characters = {}

class Starfall():
    """distributes various information regarding TGRP characters."""
    def __init__(self, bot):
        self.bot = bot
        self.dir = plugin_data_dir
        
        reader = csv.DictReader(open(self.dir + 'starfall.csv'))
        for row in reader: # http://stackoverflow.com/a/14091464
            key = row.pop('Name')
            if key in characters:
                # implement your duplicate row handling here
                pass
            characters[key] = row
            
    @commands.group(pass_context=True)
    async def starfall(self, ctx):
        """displays requested information about starfall."""
        if ctx.invoked_subcommand is None:
            await self.bot.say('incorrect starfall subcommand passed.')
            
    @starfall.command()
    async def check(self, char, stat):
        """returns the `stat` value for the provided `char`."""
        _char = char.lower().title()
        _stat = stat.lower().title()
        if _char not in characters:
            await self.bot.say("that character doesn't exist.")
        elif _stat not in characters[_char]:
                await self.bot.say("that stat doesn't exist.")
        else:
            await self.bot.say("{0} for {1}: {2}".format(_stat, _char, characters[_char][_stat]))
            
def setup(bot):
    bot.add_cog(Starfall(bot))            
