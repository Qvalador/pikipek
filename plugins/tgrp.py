# -*- coding: utf-8 -*-
#TGRP v1.1
import asyncio

from discord.ext import commands
from discord import Channel
import csv
import requests
from conf import *

characters = {}

class TGRP():
    """distributes various information regarding TGRP characters."""
    def __init__(self, bot):
        self.bot = bot
        self.dir = plugin_data_dir
        with open(self.dir + "tgrp.csv") as f:
            reader = csv.reader(f)
            for line in reader:
                characters[line[1]] = {"User": "", "Rank": "", "Faction": "", "Alias": "", "Wiki": "", "Species": "", "RC": ""}
                characters[line[1]]["User"] = line[0]
                characters[line[1]]["Rank"] = line[8]
                characters[line[1]]["Faction"] = line[3]
                characters[line[1]]["Alias"] = line[2]
                characters[line[1]]["Species"] = line[6]
                characters[line[1]]["RC"] = line[7]
                characters[line[1]]["Squad"] = line[4]
                
    @commands.group(pass_context=True)
    async def tgrp(self, ctx):
        """displays requested information about tgrp."""
        if ctx.invoked_subcommand is None:
            await self.bot.say('incorrect tgrp subcommand passed.')
                
    @commands.command()
    async def user(self, first, last=None):
        """returns the roleplayer of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["User"]
            await self.bot.say("http://reddit.com/u/" + query.title())
        elif input not in characters:
            await self.bot.say("invalid query!")
        
    @commands.command()
    async def rank(self, first, last=None):
        """returns the rating of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["Rank"]
            await self.bot.say(query)
        elif input not in characters:
            await self.bot.say("invalid query!")
            
    @commands.command()
    async def species(self, first, last=None):
        """returns the species of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["Species"]
            await self.bot.say(query.lower())
        elif input not in characters:
            await self.bot.say("Invalid query!")    
            
    @commands.command()
    async def rating(self, first, last=None):
        """returns the rating of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["Rank"]
            await self.bot.say(query.lower())
        elif input not in characters:
            await self.bot.say("invalid query!")
            
    @commands.command()
    async def faction(self, first, last=None):
        """returns the faction of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["Faction"]
            await self.bot.say(query.lower())
        elif input not in characters:
            await self.bot.say("invalid query!")
            
    @commands.command()
    async def alias(self, first, last=None):
        """returns the alias of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["Alias"]
            await self.bot.say(query.lower())
        elif input not in characters:
            await self.bot.say("invalid query!")
            
    @commands.command()
    async def rc(self, first, last=None):
        """returns the RC-type of the character specified.  if the character specified is human, returns `Human`."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["RC"]
            if query == "":
                await self.bot.say("human.")
            else:
                await self.bot.say(query.lower())
        elif input not in characters:
            await self.bot.say("invalid query!")
            
    @commands.command()
    async def squad(self, first, last=None):
        """returns the squad of the character specified."""
        query = None
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first.title()
        if input in characters:
            query = characters[input]["Squad"]
            if query == "":
                await self.bot.say("none.")
            else:
                await self.bot.say(query.lower())
        else:
            await self.bot.say("invalid query!")          
            
    @commands.command()
    async def wiki(self, first, last=None):
        """generates a wiki link for the character specified.  Basically broken."""
        if last:
            input = first + " " + last
            input = input.title()
        else:
            input = first    
        if last == "Komoto":
            last = "Koumoto"
            await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/" + first + "_" + last)
        else:
            await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/" + first + "_" + last)

    @commands.command()
    async def smut(self, input):
        """( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)( ͡° ͜ʖ ͡°)"""
        if input.lower() == "jam":
            await self.bot.say("( ͡° ͜ʖ ͡°) http://reddit.com/r/JamRP")
        if input.lower() == "nikki":
            await self.bot.say("( ͡° ͜ʖ ͡°) https://www.youtube.com/watch?v=SBWA3nh39_M")
        if input.lower() == "sierra":
            await self.bot.say("( ͡° ͜ʖ ͡°) https://www.reddit.com/r/tgrp/comments/3tvcg0/oneshot_hot_coffee")
        if input.lower() == "girthypecker":
            await self.bot.say("( ͡° ͜ʖ ͡°) https://www.reddit.com/r/tgrp/comments/4id81w/kichirou_makes_haruna_not_shit")
        if input.lower() == "kichirou":
            await self.bot.say("( ͡° ͜ʖ ͡°) http://i.imgur.com/RfzFdOy.jpg")
        if input.lower() == "ren":
            await self.bot.say("( ͡° ͜ʖ ͡°) https://en.wikipedia.org/wiki/Whisky")
        if input.lower() == "velvet":
            await self.bot.say("( ͡° ͜ʖ ͡°) https://en.wikipedia.org/wiki/The_Velveteen_Rabbit")
        if input.lower() == "kristof":
            await self.bot.say("( ͡° ͜ʖ ͡°) http://reddit.com/r/KristofRP")
        if input.lower() == "scorpion":
            await self.bot.say("_masturbates_")
        
    @commands.command()
    async def arc(self, input=None):
        """Returns the wiki link for the given arc.  Note that the argument must be given in quotations if it contains multiple words."""
        if input:
            if input.lower() in ("newcomers", "the newcomers"):
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/The_Newcomers")
            elif input.lower() in ("raiding the railroad", "railroad"):
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Raiding_the_Railroad")
            elif input.lower() == "corpse collector":
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Corpse_Collector")
            elif input.lower() == "wintertide":
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Wintertide")
            elif input.lower() in ("eye for an eye", "efae"):
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Eye_for_an_Eye")
            elif input.lower() == "candlelight":
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Candlelight")
            elif input.lower() in ("wild, wild east", "wild wild east", "wwe"):
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Wild,_Wild_East")
            elif input.lower() == "starfall":
                await self.bot.say("http://tokyo-ghoul-roleplay.wikia.com/wiki/Starfall")
        elif input is None:
            await self.bot.say("""http://tokyo-ghoul-roleplay.wikia.com/wiki/The_Newcomers
http://tokyo-ghoul-roleplay.wikia.com/wiki/Raiding_the_Railroad
http://tokyo-ghoul-roleplay.wikia.com/wiki/Corpse_Collector
http://tokyo-ghoul-roleplay.wikia.com/wiki/Wintertide
http://tokyo-ghoul-roleplay.wikia.com/wiki/Eye_for_an_Eye
http://tokyo-ghoul-roleplay.wikia.com/wiki/Candlelight
http://tokyo-ghoul-roleplay.wikia.com/wiki/Wild,_Wild_East
http://tokyo-ghoul-roleplay.wikia.com/wiki/Starfall""")
                               
        else:
            await self.bot.say("arc not found.")

def setup(bot):
    bot.add_cog(TGRP(bot))
