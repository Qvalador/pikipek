# -*- coding: utf-8 -*-

from discord.ext import commands
import discord.utils
from conf import *

def load_admins():
    admin_dir = admins_dir
    with open(admin_dir) as f:
        admins = f.readlines()
    admins = [x.strip() for x in admins]
    return admins

admins = load_admins()
        
def is_owner_check(message):
    return message.author.id == '117662741413625859'

def is_owner():
    return commands.check(lambda ctx: is_owner_check(ctx.message))

def is_admin_check(ctx):
    return ctx.author.id in admins

def is_admin():
    return commands.check(lambda ctx: is_admin_check(ctx.message))

    ch = msg.channel
    author = msg.author
    resolved = ch.permissions_for(author)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())

def has_role(member, query):
    return query in member.roles

def is_in_servers(*server_ids):
    def predicate(ctx):
        server = ctx.message.server
        if server is None:
            return False
        return server.id in server_ids
    return commands.check(predicate)
